//
//  ResponseBaseModel.swift
//  Dermacy
//
//  Created by WC-64 on 11/08/21.
//

import Foundation

struct ResponseBase<T: Codable>:Codable {
    var data: T?
    let errorcode:Int?
    let message:String?
    
    private enum CodingKeys: String, CodingKey {
        case data = "Data", errorcode = "ErrorCode", message = "Message"
    }
}
