//
//  IntrinsicTableView.swift
//  masho
//
//  Created by Appzoc on 19/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class IntrinsicTableView: UITableView {

    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height:    contentSize.height)
    }

}
