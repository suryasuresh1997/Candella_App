//
//  BrowserClient.swift
//  josco
//
//  Created by WC-64 on 07/08/21.
//  Copyright © 2021 Josco. All rights reserved.
//

import UIKit
import SafariServices

class BrowserClient {
    
   
    class func openSafariController(path:String) {
        guard let topVC = UIApplication.topMostViewController else { return }
        if let url = URL(string: path) {
            let controller = SFSafariViewController(url: url)
//            controller.preferredBarTintColor = UIColor.
//            controller.preferredControlTintColor = UIColor.groupTableViewBackground
            controller.dismissButtonStyle = .close
            controller.configuration.barCollapsingEnabled = true
            topVC.present(controller, animated: true, completion: nil)
        }
    }
    
    
    class func canOpen(path:String) {
        if let url = URL(string: "https://www.hackingwithswift.com") {
            UIApplication.shared.open(url)
        }
    }
    
}
