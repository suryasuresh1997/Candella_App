//
//  Preference.swift
//  Dermacy
//

import Foundation

enum PersisnatKey: String {
    case uID = "uID"
    case user = "user"
    case token = "token"
    case language = "language"
    case session = "session"
    case user_name = "name"
    case phone_number = "phone"
    case email = "email"
    case guest = "false"
}

class LocalPersistant {
    
    //MARK: STATIC PROPERTY
    private static let storage = UserDefaults.standard
    
    
    //MARK: - SAVE TO PERSISTANT STORE
    static func save(value: Any, key: PersisnatKey) {
        storage.set(value, forKey: key.rawValue)
    }
    
    static func bool(for key: PersisnatKey) -> Bool {
        return storage.bool(forKey: key.rawValue)
    }
    
    static func string(for key: PersisnatKey) -> String? {
        return storage.string(forKey: key.rawValue)
    }
    static func int(for key: PersisnatKey) -> Int? {
        return storage.integer(forKey: key.rawValue)
    }
    
    static func data(for key: PersisnatKey) -> Data? {
           return storage.data(forKey: key.rawValue)
       }
    
    //MAKR: - GET OBJECT FROM PERSISTANT STORE
    //NOTE:  - OBJECT ACCESSED FOR KEY AND RETURNS OBJECT 
//    static func object(for key: PersisnatKey) -> Location? {
//
//        var location: Location?
//        if let data = storage.data(forKey: key.rawValue) {
//            do {
//                let decoder = JSONDecoder()
//                let kLocation = try decoder.decode(Location.self, from: data)
//                location = kLocation
//            } catch {
//                print("Unable to Decode Note (\(error))")
//            }
//        }
//        return location
//    }
 
}
