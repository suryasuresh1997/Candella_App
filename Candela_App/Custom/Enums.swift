//
//  Enums.swift
//  Dermacy
//
//  Created by WC-64 on 19/07/21.
//

import Foundation


enum ViewType : String{
    case cart = "Cart"
    case orderSummery = "Order Summary"
}

enum AddressViewType {
    case normal,fromcart
}

enum ViewEmptyResult {
    case search
    case wishlist
    case notification
}
