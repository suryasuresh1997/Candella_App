//
//  Protocols.swift
//  Dermacy
//
//  Created by WC-64 on 19/07/21.
//

import Foundation

//
//@objc public protocol GrowingTextViewDelegate: UITextViewDelegate {
//    @objc optional func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat)
//    @objc optional func textViewDidChangeText(characterCount: Int)
//
//}
//



@objc public protocol NotifyDelegate: AnyObject {
    @objc optional func didSelect(withInfo info: [String:Any]?)
}

@objc public protocol ProductCellDelegate : AnyObject {
    
    @objc optional func onSubmitQty(withInfo info:[String:Any])
    @objc optional func changeMeasurement(withInfo info: [String:Any]?)
    @objc optional func onClickFav(withInfo info: [String:Any]?)
    @objc optional func onClickAddProduct(withInfo info: [String:Any]?)
    @objc optional func onClickRemoveProduct(withInfo info: [String:Any]?)
    
    
}

protocol CartAddressChangeDelegate : AnyObject {
    func onChangeAddress(withInfo info:[String:Any])
}

protocol EmptyViewDelegate:AnyObject {
    func didSelectEmptyView(withInfo info:[String:Any])
}
